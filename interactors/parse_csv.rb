# frozen_string_literal: true

require 'csv'
require 'interactor'

class ParseCSV
  include Interactor

  def call
    csv = CSV.read(context.filename, headers: true, col_sep: ';')
    context.orders = csv.group_by { |row| row['Order Number'] }.each_value.map do |rows|
      row = rows.first
      {
        order_number: row['Order Number'],
        created_at: row['Created at'].split.first,
        invoice_number: 'invoice_number',

        shipping_name: row['Shipping Name'],
        shipping_address1: [row['Shipping Address'], row['Shipping Address2']].join(' '),
        shipping_address2: [row['Shipping Address4'].to_s.split('/').first, row['Shipping Address3'].to_s.split('/').first, row['Shipping Address5']].join(' '),
        shipping_phone: row['Shipping Phone Number'],

        billing_name: row['Billing Name'],
        billing_address1: [row['Billing Address'], row['Shipping Address2']].join(' '),
        billing_address2: [row['Billing Address4'].to_s.split('/').first, row['Shipping Address3'].to_s.split('/').first, row['Shipping Address5']].join(' '),
        billing_phone: row['Billing Phone Number'],

        email: row['Customer Email'],
        tax_code: row['Tax Code'],

        description: rows.map { |r| [r['Item Name'], r['Paid Price']] }
      }
    end
  end
end

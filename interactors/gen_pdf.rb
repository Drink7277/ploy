# frozen_string_literal: true

require 'interactor'
require 'rubyXL'
require 'rubyXL/convenience_methods/cell'
require 'baht'
require 'libreconv'

class GenPDF
  include Interactor

  delegate :order, :output, to: :context

  delegate :ind2ref, :ref2ind, to: RubyXL::Reference

  def call
    workbook = RubyXL::Parser.parse('./assets/reference.xlsx')
    workbook.calc_pr.full_calc_on_load = true
    sheet = workbook[0]

    sheet[12][4].change_contents order[:created_at]
    sheet[13][4].change_contents order[:invoice_number]

    sheet[16][4].change_contents order[:billing_name]
    sheet[17][4].change_contents order[:billing_address1]
    sheet[18][4].change_contents order[:billing_address2]
    sheet[19][4].change_contents order[:tax_code]
    sheet[20][4].change_contents order[:shipping_phone]
    sheet[21][4].change_contents order[:email]

    sheet[24][4].change_contents order[:shipping_name]
    sheet[25][4].change_contents order[:shipping_address1]
    sheet[26][4].change_contents order[:shipping_address2]
    sheet[27][4].change_contents order[:tax_code]
    sheet[28][4].change_contents order[:email]

    order[:description].each_with_index do |(des, price), i|
      sheet[38 + i][1].change_contents "#{i + 1}."
      sheet[38 + i][2].change_contents des
      sheet[38 + i][9].change_contents price.to_f
    end

    total = order[:description].map(&:second).map(&:to_f).sum
    vat = total * 0.07
    total_and_vat = total + vat

    sheet[55][9].change_contents total
    sheet[56][9].change_contents vat
    sheet[57][9].change_contents total_and_vat
    sheet[63][5].change_contents Baht.words(total_and_vat)

    FileUtils.mkdir_p File.join(output, 'xlsx')
    FileUtils.mkdir_p File.join(output, 'pdf')

    xlsx_file = File.join(output, 'xlsx', "#{order[:order_number]}.xlsx")
    pdf_file = File.join(output, 'pdf', "#{order[:order_number]}.pdf")

    workbook.write(xlsx_file)
    Libreconv.convert(xlsx_file, pdf_file)
  end
end

# frozen_string_literal: true

require 'pry'
require 'active_support'
require 'active_support/core_ext'
require 'csv'
require './interactors/parse_csv'
require './interactors/gen_pdf'
require 'progress_bar'

orders = ::ParseCSV.call(filename: './input.csv').orders

bar = ProgressBar.new(orders.size)

orders.each do |order|
  GenPDF.call(order: order, output: './output')
  bar.increment!
end

puts '==============='
